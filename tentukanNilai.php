<?php
function tentukan_nilai($number)
{
  if($number>90){
      echo "nilai = ".$number." sangat baik <br>";
  }else if($number>70){
    echo "nilai = ".$number." baik <br>";
  }else if($number>60){
      echo "nilai = ".$number." cukup <br>";
  }else if($number<60){
      echo "nilai = ".$number." kurang <br>";
  }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>