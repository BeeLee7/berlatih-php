<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo $sheep->name.'<br>'; 
echo $sheep->legs."<br>"; 
echo $sheep->cold_blooded."<br>"; 

$sungokong = new Ape("kera sakti");
$sungokong->yell();
echo "<br>".$sungokong->name.'<br>';
echo $sungokong->legs.'<br>';
echo $sungokong->cold_blooded.'<br>';


$kodok = new Frog("buduk");
$kodok->jump() ; 
echo "<br>".$kodok->name.'<br>';
echo $kodok->legs.'<br>';
echo $kodok->cold_blooded.'<br>';


?>