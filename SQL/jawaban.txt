//Nomor 1
CREATE DATABASE myshop;


//Nomor 2
use myshop;
CREATE TABLE users(
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(255), 
email VARCHAR(255), 
password VARCHAR(255));

CREATE TABLE categories(
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(255));

CREATE TABLE items(
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(255), 
description VARCHAR(255), 
price INT, 
stock INT, 
category_id INT, FOREIGN KEY(category_id) REFERENCES categories(id));


//Nomor 3
INSERT INTO users (name,email,password)
VALUES 
('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO categories (name)
VALUES 
('gadget'),
('cloth'),
('men'),
('women'),
('branded');

INSERT INTO items (name,description,price,stock,category_id)
VALUES 
('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
('IMHO WATCH', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


//Nomor 4
SELECT id,name,email FROM users;

SELECT * FROM items WHERE price<1000000;
SELECT * FROM items WHERE name LIKE '%sang%';

SELECT items.name,items.description,items.price,items.stock,items.category_id,categories.name as kategori
FROM items
INNER JOIN categories
ON items.category_id = categories.id;


//Nomor 5
UPDATE items SET price=2500000 WHERE name = 'Sumsang b50';
