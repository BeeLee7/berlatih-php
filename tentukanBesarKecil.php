<?php
function tukar_besar_kecil($string){
//kode di sini
    $katabaru = '';
    for ($i=0 ; $i<strlen($string) ; $i++) {
        if ($string[$i] >= 'A' && $string[$i] <= 'Z') {
            $katabaru .= strtolower($string[$i]);
        } else if ($string[$i] >= 'a' && $string[$i] <= 'z') {
            $katabaru .= strtoupper($string[$i]);
        } else {
            $katabaru .=$string[$i];
        }
    }
    echo $string. " => ";
    echo $katabaru."<br>";
    
    
    
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>